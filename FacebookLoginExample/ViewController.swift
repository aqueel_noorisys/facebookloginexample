//
//  ViewController.swift
//  FacebookLoginExample
//
//  Created by Nsys on 20/07/20.
//  Copyright © 2020 Nsys. All rights reserved.
//

import UIKit
import FBSDKLoginKit

class ViewController: UIViewController, LoginButtonDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let loginButton = FBLoginButton()
        loginButton.center = view.center
        loginButton.delegate = self
        view.addSubview(loginButton)
        
        loginButton.permissions = ["public_profile", "email"]
    }
    
    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
        
        if error == nil
        {
            print("login completed...")
//            print("Result User Id ====== " + (result?.token!.userID)!)
            
            let fbRequest = GraphRequest(graphPath: "me",
                                      parameters: ["fields": "id, name, first_name, last_name, email, picture.type(large)"],
                                      tokenString: AccessToken.current?.tokenString,
                                      version: nil,
                                      httpMethod: HTTPMethod(rawValue: "GET"))

            fbRequest.start(completionHandler: { test, result, error in
                if(error == nil) {
                    print("result =====  \(result)")
                    
                    guard let json = result as? NSDictionary else { return }
                    if let email = json["email"] as? String {
                        print("Email ==== \(email)")
                    }
                    if let firstName = json["first_name"] as? String {
                        print("First Name ==== \(firstName)")
                    }
                    if let lastName = json["last_name"] as? String {
                        print("Last Name ==== \(lastName)")
                    }
                    if let id = json["id"] as? String {
                        print("Id ==== \(id)")
                    }
                    if let imageURL = ((json["picture"] as? [String: Any])?["data"] as? [String: Any])?["url"] as? String {
                         print("Picture ==== \(imageURL)")
                    }
                    
                    
                } else {
                     print("Result error ===== \(error)")
                }
            })
            
        }
        else
        {
            print("FBLogin Error ======= " + error!.localizedDescription)
        }
    }
    
    func loginButtonWillLogin(_ loginButton: FBLoginButton) -> Bool {
        print("User Loged in...")
        
        return true
    }
    
    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
        print("User Loged out...")
    }

   
}

